= ${document.title}
:author:
:revnumber: ${project.version} (${timestamp})
// Settings:
:experimental:
:reproducible:
:icons: font
:icon-set: fas
:sectlinks: true
:sectnums: true
:sectanchors: true
:toc: left
:toclevels: 4
:toc-position: left
:toc-title: Inhalt
:chapter-label:
:figure-caption: Abbildung
:tof-caption: Abbildung
:tot-caption: Tabelle
:preface-caption: Vorwort
:lang: DE
:hyphen:
:hyphens: de
:imagesdir: images
:source-highlighter: rouge
:rouge-style: github
ifdef::backend-pdf[]
:pdf-page-size: A4
:pdf-theme: funkfeuer
:pdf-themesdir: {docdir}
:pdf-page-mode: none
:title-page: true
:pagenums: true
:outlinelevels: 4:1
:idprefix: true
:idseparator: -
:hide-uri-scheme: true
:compress:
endif::[]

<<<

[[test]]
== TEST

=== Passthrough Test 1

.A meaningful label 1
passthrough:screenshots/redeemer-portal-01-login.png[width=80%,align="center"]
