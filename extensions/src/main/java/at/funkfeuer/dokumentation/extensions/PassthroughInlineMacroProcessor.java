package at.funkfeuer.dokumentation.extensions;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.*;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.log.Severity;

import java.util.HashMap;
import java.util.Map;

@Name("passthrough")
@Format(FormatType.LONG)
public class PassthroughInlineMacroProcessor extends InlineMacroProcessor {

    public PassthroughInlineMacroProcessor() {
        this(null);
    }

    public PassthroughInlineMacroProcessor(final String macroName) {
        this(macroName, new HashMap<>());
    }

    public PassthroughInlineMacroProcessor(final String macroName, final Map<String, Object> config) {
        super(macroName, config);
    }

    @Override
    public Object process(final ContentNode parent, final String target, final Map<String, Object> attributes) {
        // Passthrough image
        try {
            // Process asciidoc
            log(Severity.DEBUG, "TARGET: " + target);

            final Map<String, Object> options = new HashMap<>();
            options.put("type", "image");
            options.put("target", target);

            final Map<String, Object> attrs  = new HashMap<>();
            attrs.putAll(attributes);
            if (!attrs.containsKey("alt")) {
                attrs.put("alt", "");
            }
            return createPhraseNode(parent, "image", "", attrs, options).convert();
        } catch (Exception ex) {
            log(Severity.ERROR, "Error while processing 'passthrough' macro. Message: " + ExceptionUtils.getStackTrace(ex));
        }
        return null;
    }

    private void log(final Severity severity, final String message) {
        try {
            log(new LogRecord(severity, message));
        } catch (Exception ex) {
            //
        }
    }
}






