package at.funkfeuer.dokumentation.extensions;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.extension.JavaExtensionRegistry;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

public class Registry implements ExtensionRegistry {

    @Override
    public void register(final Asciidoctor asciidoctor) {
        final JavaExtensionRegistry javaExtensionRegistry = asciidoctor.javaExtensionRegistry();

        javaExtensionRegistry.inlineMacro(PassthroughInlineMacroProcessor.class);
    }
}
