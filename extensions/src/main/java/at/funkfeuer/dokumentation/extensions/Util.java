package at.funkfeuer.dokumentation.extensions;

import org.apache.commons.lang3.StringUtils;
import org.asciidoctor.ast.ContentNode;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class Util {

	private Util() {
		//
	}

	public static String getStringAttributeValue(final Map<String, Object> attributes, final String key) {
		if (attributes == null || StringUtils.isBlank(key)) {
			return null;
		}

		final Object value = attributes.get(StringUtils.trim(key));
		if (value != null) {
			if (value instanceof String) {
				return (String) value;
			} else {
				return value.toString();
			}
		}

		return null;
	}

	public static Boolean getBoolAttributeValue(final Map<String, Object> attributes, final String key) {
		if (attributes == null || StringUtils.isBlank(key)) {
			return null;
		}

		final Object value = attributes.get(StringUtils.trim(key));
		if (value != null) {
			if (value instanceof String) {
				return Boolean.parseBoolean((String) value);
			} else if (value instanceof Boolean) {
				return (Boolean) value;
			} else {
				return Boolean.parseBoolean(value.toString());
			}
		}

		return null;
	}

	public static Path getImagesDirPath(final ContentNode contentNode) {
		if (contentNode == null) {
			return null;
		}

		final String outDir = (String) contentNode.getDocument().getAttributes().get("outdir");
		final Path outDirPath = Paths.get(outDir).normalize();
		//log(Severity.DEBUG, "OUTDIR PATH: " + outDirPath);

		final String imagesDir = (String) contentNode.getDocument().getAttributes().get("imagesdir");
		final Path imagesDirPath = outDirPath.resolve(imagesDir).normalize();
		//log(Severity.DEBUG, "IMAGESDIR PATH: " + imagesDirPath);

		return imagesDirPath;
	}

}
